//
//  LogInViewCntrl.swift
//  TestMaps
//
//  Created by Stanislav on 19.08.17.
//  Copyright © 2017 Stanislav. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import SwiftSpinner



class LogInViewCntrl: UIViewController {
	
	// MARK: - Outlets
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	// MARK: - IBActions
	@IBAction private func createAccount() {
		if let email = emailTextField.text, let password = passwordTextField.text {
			SwiftSpinner.show("")
			Auth.auth().createUser(withEmail: email, password: password, completion:
				{ [weak self] (user, error) in
				guard let strongSelf = self else { return }
					SwiftSpinner.hide()
				if let firebaseError = error {
					print(firebaseError.localizedDescription)
					return
				}
					UIAlertController.notifyUser("", message: "UserCreated", alertButtonTitles: ["Ok"],					                             alertButtonStyles: [.cancel], vc: strongSelf, completion: {_ in })
				
			})
		}
	}
	
	@IBAction private func login() {
		if let email = emailTextField.text, let password = passwordTextField.text {
			SwiftSpinner.show("")
			Auth.auth().signIn(withEmail: email, password: password, completion: { [weak self](user, error) in
				guard let strongSelf = self else { return }
				SwiftSpinner.hide()
				if let firebaseError = error {
					print(firebaseError.localizedDescription)
					return
				}
				let mapViewCntrl = strongSelf.storyboard?.instantiateViewController(withIdentifier: "MapViewCntrl")
				strongSelf.show(mapViewCntrl!, sender: strongSelf)
			})
		}
	}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
