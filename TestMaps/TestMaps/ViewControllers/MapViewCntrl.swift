//
//  MapViewCntrl.swift
//  TestMaps
//
//  Created by Stanislav on 19.08.17.
//  Copyright © 2017 Stanislav. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth
import SDWebImage
import SwiftSpinner
import SwipeView





class MapViewCntrl: UIViewController {
	
	// MARK: - Outlets
	@IBOutlet weak var mapView: GMSMapView!
	@IBOutlet weak var locationTextField: UITextField!
	@IBOutlet weak var selector: NPSegmentedControl!
	@IBOutlet weak var avatarButton: UIButton!
	@IBOutlet weak var swipeView: SwipeView!
	
	
	// MARK: - vars
//	//location manager
	var locationManager = CLLocationManager()
	var imagePicker = UIImagePickerController()
	var storageRef = Storage.storage().reference()
	var databaseRef = Database.database().reference()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		initLocationTextField()
		initializeSegment()
		initializeImagePicker()
		initLocationManager()
		initGoogleMaps()
		initSwipeView()
		
        // Do any additional setup after loading the view.
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		presentAvatar()
	}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	// MARK: - private functions
	
	fileprivate func initializeSegment() {
		let myElements = ["По расстоянию", "По стоимости"]
		selector.cursor = UIImageView(image: UIImage(named: "icon_arrow"))
		selector.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
		selector.unselectedFont = UIFont(name: "HelveticaNeue-Light", size: 16)
		selector.selectedFont = UIFont(name: "HelveticaNeue-Bold", size: 16)
		selector.unselectedTextColor = UIColor(red: 98/255, green: 98/255, blue: 98/255, alpha: 1)
		selector.unselectedColor = UIColor(red: 42/255, green: 42/255, blue: 42/255, alpha:1)
		selector.selectedTextColor = UIColor(white: 1, alpha: 1)
		selector.selectedColor = UIColor(red: 42/255, green: 42/255, blue: 42/255, alpha: 1)
		selector.cursorPosition = CursorPosition.Bottom
		selector.setItems(items: myElements)
	}
	
	fileprivate func initializeImagePicker() {
		imagePicker.delegate = self
		imagePicker.allowsEditing = true
	}
	fileprivate func initLocationManager() {
		locationManager = CLLocationManager()
		locationManager.delegate = self
		locationManager.requestWhenInUseAuthorization()
		locationManager.startUpdatingLocation()
		locationManager.startMonitoringSignificantLocationChanges()
	}
	fileprivate func initLocationTextField() {
		locationTextField.addLeftPadding()
		locationTextField.addRightPadding()
		locationTextField.delegate = self
	}
	fileprivate func initSwipeView() {
		swipeView.dataSource = self
		swipeView.delegate = self
		swipeView.bounces = false
	}
	fileprivate func initGoogleMaps() {
		// Create a GMSCameraPosition that tells the map to display the
		// coordinate -33.86,151.20 at zoom level 6.
		let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
		let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
		mapView.isMyLocationEnabled = true
		self.mapView.camera = camera
		
		self.mapView.delegate = self
		self.mapView.isMyLocationEnabled = true
		self.mapView.settings.myLocationButton = true
		
		// Creates a marker in the center of the map.
		let marker = GMSMarker()
		marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
		marker.map = mapView
	}
	
	fileprivate func presentAvatar() {
		guard let user = Auth.auth().currentUser else { return }
		
		databaseRef.child("users").child(user.uid).observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
			guard let strongSelf = self else {
				return
			}
			if let dict = snapshot.value as? [String: AnyObject] {
				if let profileImageUrl = dict["pic"] as? String {
					if !profileImageUrl.isEmpty {
						strongSelf.avatarButton.sd_setImage(with: URL(string: profileImageUrl), for: .normal, placeholderImage: #imageLiteral(resourceName: "icon_user"), options: SDWebImageOptions(), completed: nil)
					}
				}
			}
		}) { (error) in
			print(error)
		}
		
	}
	
	// MARK: - IBActions
	@IBAction private func selectorValueChanged(_ sender: NPSegmentedControl) {
		swipeView.currentItemIndex = sender.selectedIndex
	}
	
	@IBAction private func createAvatar(_ sender: Any) {
		
		CameraUtil.openCameraActionSheet(imagePicker: imagePicker, holderController: self)
	}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - GMSMapViewDelegate
extension MapViewCntrl: GMSMapViewDelegate {
	func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
		self.mapView.isMyLocationEnabled = true
	}
	func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
		self.mapView.isMyLocationEnabled = true
		if gesture {
			self.mapView.selectedMarker = nil
		}
	}
}
//MARK: - UITextFieldDelegate
extension MapViewCntrl: UITextFieldDelegate {
	func textFieldDidBeginEditing(_ textField: UITextField) {
		let autoCompleteController = GMSAutocompleteViewController()
		autoCompleteController.delegate = self
		locationManager.startUpdatingLocation()
		self.present(autoCompleteController, animated: true, completion: nil)
	}
}
//MARK: - GMSAutocompleteViewControllerDelegate
extension MapViewCntrl: GMSAutocompleteViewControllerDelegate {
	func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
		let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15)
		self.mapView.camera = camera
		self.mapView.clear()
		let marker = GMSMarker()
		marker.position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
		marker.map = mapView
		locationTextField.text = place.name
		self.dismiss(animated: true, completion: nil)
	}
	func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
		print(error)
	}
	func wasCancelled(_ viewController: GMSAutocompleteViewController) {
		locationTextField.text = ""
		self.dismiss(animated: true, completion: nil)
	}
}

//MARK: - CLLocationManagerDelegate
extension MapViewCntrl: CLLocationManagerDelegate {
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		let location = locations.last
		let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17)
		self.mapView.animate(to: camera)
		self.locationManager.stopUpdatingLocation()
	}
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		print(error)
	}
}
 //MARK: - UIImagePickerControllerDelegate
extension MapViewCntrl: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
			
			saveImage(image: pickedImage.resizeImageWith(newSize: CGSize(width: 24, height: 24)))
		}
		
		
	}
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		dismiss(animated: true, completion: nil)
		
	}
	
	private func saveImage(image: UIImage) {
		SwiftSpinner.show("")
		let imageName = NSUUID().uuidString
		let storedImage = storageRef.child("profile_images").child(imageName)
		if let uploadData = UIImageJPEGRepresentation(image, 1) {
			storedImage.putData(uploadData, metadata: nil, completion: { [weak self](metaData, error) in
				if let error = error{
					print(error)
					return
				}
				guard let strongSelf = self else { return }
				if let absoluteUrl = metaData?.downloadURL()?.absoluteString {
					strongSelf.databaseRef.child("users").child((Auth.auth().currentUser?.uid)!).updateChildValues(["pic": absoluteUrl], withCompletionBlock: { [weak self] (error, ref) in
						if let error = error{
							print(error)
							return
						}
						guard let strongSelf = self else { return }
						SwiftSpinner.hide()
						strongSelf.dismiss(animated: true, completion: nil)
					})
				}
	
			})
		}
		
	}
	
}
