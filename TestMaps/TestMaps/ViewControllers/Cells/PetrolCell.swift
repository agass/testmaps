//
//  PetrolCell.swift
//  TestMaps
//
//  Created by Stanislav on 20.08.17.
//  Copyright © 2017 Stanislav. All rights reserved.
//

import UIKit

class PetrolCell: UITableViewCell {
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var adressLabel: UILabel!
	@IBOutlet weak var distanceLabel: UILabel!
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var avatarImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
