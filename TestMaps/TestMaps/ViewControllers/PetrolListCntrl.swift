//
//  PetrolListCntrl.swift
//  TestMaps
//
//  Created by Stanislav on 20.08.17.
//  Copyright © 2017 Stanislav. All rights reserved.
//

import UIKit

class PetrolListCntrl: UIViewController {
	
	// MARK: - Outlets
	@IBOutlet weak var tableView: UITableView!
	
	var petrols = [Petrol]()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		tableView.register(UINib(nibName: "PetrolCell", bundle: nil), forCellReuseIdentifier: "PetrolCell")
		let petrol1  = Petrol()
		petrol1.title = "Автозапрвка Shell"
		petrol1.distance = "1.6 KM"
		petrol1.time  = "час назад"
		petrol1.address = "Садовничая, 57"
		petrol1.image = #imageLiteral(resourceName: "icon_petrol1")
		petrols.append(petrol1)
		tableView.delegate = self
		tableView.dataSource = self
		
		
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PetrolListCntrl: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return petrols.count
	}
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let petrol = petrols[indexPath.row]
		let cell = tableView.dequeueReusableCell(withIdentifier: "PetrolCell", for: indexPath) as! PetrolCell
		cell.adressLabel.text = petrol.address
		cell.titleLabel.text = petrol.title
		cell.timeLabel.text = petrol.time
		cell.avatarImageView.image = petrol.image
		return cell
	}
}
extension PetrolListCntrl: UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 60
	}
}
