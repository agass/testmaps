//
//  Petrol.swift
//  TestMaps
//
//  Created by Stanislav on 20.08.17.
//  Copyright © 2017 Stanislav. All rights reserved.
//

import Foundation
import UIKit

class Petrol {
	var title: String = ""
	var address: String = ""
	var image: UIImage = UIImage()
	var distance: String = ""
	var time: String = ""
}
