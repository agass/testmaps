//
//  MapViewCntrl+SwipeView.swift
//  TestMaps
//
//  Created by Stanislav on 21.08.17.
//  Copyright © 2017 Stanislav. All rights reserved.
//

import Foundation
import SwipeView

extension MapViewCntrl: SwipeViewDataSource {
	
	var cntls: [UIViewController] {
		get {
			let petrolListCntrl = self.storyboard?.instantiateViewController(withIdentifier: "PetrolListCntrl") as! PetrolListCntrl
			let petrolListCntrlPrice = self.storyboard?.instantiateViewController(withIdentifier: "PetrolListCntrlPrice")
			addChildViewController(petrolListCntrl)
			addChildViewController(petrolListCntrlPrice!);
			return [petrolListCntrl, petrolListCntrlPrice!]
		}
	}
	
	func numberOfItems(in swipeView: SwipeView!) -> Int {
		return cntls.count
	}
	
	func swipeView(_ swipeView: SwipeView!, viewForItemAt index: Int, reusing view: UIView!) -> UIView! {
		
		let ctrl = cntls[index] as UIViewController;
		ctrl.view.frame = self.swipeView.frame
		return ctrl.view
	
	}
}

extension MapViewCntrl: SwipeViewDelegate {
	func swipeViewCurrentItemIndexDidChange(_ swipeView: SwipeView!) {
		selector.selectCell(index: swipeView.currentItemIndex, animate: true)
	}
}
