//
//  UIAlertController+Extension.swift
//  TestMaps
//
//  Created by Stanislav on 19.08.17.
//  Copyright © 2017 Stanislav. All rights reserved.
//

import UIKit


extension UIAlertController {
	
	typealias CompletionHandlerClosureType = (Int) -> Void
	static func notifyUser(_ title: String, message: String, alertButtonTitles: [String], alertButtonStyles: [UIAlertActionStyle], vc: UIViewController, completion: @escaping (Int)->Void) -> Void
	{
		let alert = UIAlertController(title: title,
		                              message: message,
		                              preferredStyle: UIAlertControllerStyle.alert)
		
		for title in alertButtonTitles {
			let actionObj = UIAlertAction(title: title,
			                              style: alertButtonStyles[alertButtonTitles.index(of: title)!], handler: { action in
											completion(alertButtonTitles.index(of: action.title!)!)
			})
			
			alert.addAction(actionObj)
		}
		
		
		//vc will be the view controller on which you will present your alert as you cannot use self because this method is static.
		vc.present(alert, animated: true, completion: nil)
	}
}
