//
//  UITextField+Extension.swift
//  TestMaps
//
//  Created by Stanislav on 20.08.17.
//  Copyright © 2017 Stanislav. All rights reserved.
//

import UIKit

extension UITextField {
	
	func addLeftPadding() {
		let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 28, height: 0))
		self.leftView = leftView
		self.leftViewMode = .always
	}
	func addRightPadding() {
		let rightView = UIView(frame: CGRect(x: 0, y: 0, width: 28, height: 0))
		self.rightView = rightView
		self.rightViewMode = .always
	}
}
