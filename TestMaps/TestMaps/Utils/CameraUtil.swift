//
//  CameraUtil.swift
//  TestMaps
//
//  Created by Stanislav on 20.08.17.
//  Copyright © 2017 Stanislav. All rights reserved.
//

import Foundation
import UIKit

class CameraUtil {
	
	class func openCameraActionSheet(imagePicker: UIImagePickerController, holderController: UIViewController)
	{
		let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
		
		//alert.view.tintColor = UIColor.init(red: 50/255, green: 50/255, blue: 50/255, alpha: 1.0);
		let galleryButton = UIAlertAction.init(title: "Add from gallery", style: UIAlertActionStyle.default) { (action) in
			
			imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
			imagePicker.mediaTypes = ["public.image"];
			holderController.present(imagePicker, animated: true, completion: nil)
			
		}
		
		alert.addAction(galleryButton)
		if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
			
			let cameraButton  = UIAlertAction.init(title: "Take a photo", style: UIAlertActionStyle.default, handler: { (action) in
				
				imagePicker.sourceType = UIImagePickerControllerSourceType.camera
				imagePicker.mediaTypes = ["public.image"];
				holderController.present(imagePicker, animated: true, completion: nil)
			})
			
			alert.addAction(cameraButton);
		}
		else {
			print("Camera not available")
			
		}
		
		
		let  cancelButton  = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel) { (action) in
			print("Cancel Pressed");
		}
		
		
		
		alert.addAction(cancelButton)
		
		holderController.present(alert, animated: true, completion: nil)
	}

}
